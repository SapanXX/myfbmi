import 'package:flutter/material.dart';
import 'screens/inputpage.dart';
import 'constants.dart';

void main() => runApp(BMI());

class BMI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'BMI-CALC',
      theme: ThemeData.dark().copyWith(
        primaryColor: kThemePrimaryColor,
        scaffoldBackgroundColor: kScafoldBackgroundColour,
      ),
      home: InputPage(),
    );
  }
}
