void main() {
  Car myCar = Car(drive: slowDrive);
  print("first line: ");
  print(myCar.drive);

  print("second line");
  print(myCar.drive());

  print("third line");
  myCar.drive = fastDrive;
  print(myCar.drive());
}

class Car {
  Car({this.drive: slowDrive});
  Function drive;
}

void slowDrive() {
  print("driving slowly");
}

void fastDrive() {
  print("Driving superfaast");
}
