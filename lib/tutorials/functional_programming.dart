void main() {
  int result = calculaor(5, 8, multiply);
  print(result);
}

// int calculaor(int n1, int n2, Function calculation) {
//   return calculation(n1, n2);
// }

final Function calculaor = (int n1, int n2, Function calculation) {
  return calculation(n1, n2);
};

int add(int n1, int n2) {
  return n1 + n2;
}

int multiply(int n1, int n2) {
  return n1 * n2;
}
